package top.example.service;

import top.example.model.User;


public interface UserService {

    User checkAccount(String userNumber);

    User findUserByName(String userName);

    void addUser(User newUser);

    void deleteUser(String userNumber);

    void updateUser(User newUser);
}
