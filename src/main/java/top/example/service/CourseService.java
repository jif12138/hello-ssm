package top.example.service;

import top.example.model.Course;


import java.util.List;


public interface CourseService {
    void addCourse(Course courseModel);

    void deleteCourseByNumber(String scNumber);

    void updateCourse(Course courseModel);

    Course findOneCourseByCouNumber(String stuNumber);

    List<Course> findList();

    List<Course> findListByCouTeaNumber(String stuNumber);


}
