package top.example.service;


import top.example.model.Student;

import java.util.List;

public interface StudentService {

    void addStudent(Student student);

    void deleteStudent(String studentNUmber);

    void updateStudent(Student student);

    Student findStudent(String studentNUmber);

    List<Student> findStudentList();

    List<Student> findStudentListByCouNumber(String couNumber);

    List<Student> findStudentListByClaNumber(String couNumber);

    Student selectUserByName(String studentName);
}