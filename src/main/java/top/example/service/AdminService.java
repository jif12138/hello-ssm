package top.example.service;


import top.example.model.Admin;

public interface AdminService {
    Admin findOneAdminByNumber(String adminNumber);

    Admin findOneAdminByName(String adminName);

    void updateAdmin(Admin admin);
}