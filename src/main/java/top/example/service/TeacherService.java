package top.example.service;


import top.example.model.Teacher;

import java.util.List;

public interface TeacherService {

    void addTeacher(Teacher teacher);

    void deleteTeacher(String teacherNUmber);

    void updateTeacher(Teacher teacher);

    Teacher findTeacher(String teacherNUmber);

    List<Teacher> findTeacherList();

    Teacher selectUserByName(String username);
}