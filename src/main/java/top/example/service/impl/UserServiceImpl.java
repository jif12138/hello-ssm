package top.example.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.example.mapper.UserMapper;
import top.example.model.User;
import top.example.service.UserService;


@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;


    @Override
    public User checkAccount(String userNumber) {
        try {
            User user = userMapper.selectUser(userNumber);
            System.out.println("查询下结果：" + user.toString());
            return user;
        } catch (Exception e) {
            System.out.println(e);
            return null;

        }

    }

    @Override
    public User findUserByName(String userName) {
        return userMapper.selectUserByName(userName);
    }

    @Override
    public void addUser(User user) {
        userMapper.insertUser(user);
    }

    @Override
    public void deleteUser(String userNumber) {
        userMapper.deleteUserByNumber(userNumber);
    }

    @Override
    public void updateUser(User newUser) {
        userMapper.updateUser(newUser);
    }
}
