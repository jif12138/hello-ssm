package top.example.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.example.mapper.ClassInfoMapper;
import top.example.model.ClassInfo;
import top.example.service.ClassInfoService;

import java.util.List;


@Service
public class ClassInfoServiceImpl implements ClassInfoService {

    @Autowired
    private ClassInfoMapper classInfoMapper;


    @Override
    public void updateClass(ClassInfo classModel) {
        classInfoMapper.updateClassInfo(classModel);
    }

    @Override
    public ClassInfo findOneClassByClaNumber(String stuNumber) {
        return classInfoMapper.selectOneClassByNumber(stuNumber);
    }

    @Override
    public List<ClassInfo> findList() {
        return classInfoMapper.selectList();
    }


}
