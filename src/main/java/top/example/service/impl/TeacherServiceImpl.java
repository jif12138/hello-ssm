package top.example.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.example.mapper.TeacherMapper;
import top.example.model.Teacher;
import top.example.service.TeacherService;

import java.util.List;


@Service
public class TeacherServiceImpl implements TeacherService {

    @Autowired
    private TeacherMapper teacherMapper;


    @Override
    public void addTeacher(Teacher teacher) {
        teacherMapper.insetOneTeacher(teacher);
    }

    @Override
    public void deleteTeacher(String teacherNumber) {
        teacherMapper.deleteTeacherByNumber(teacherNumber);
    }

    @Override
    public void updateTeacher(Teacher teacher) {
        teacherMapper.updateTeacherInfo(teacher);
    }

    @Override
    public Teacher findTeacher(String teaNUmber) {
        return teacherMapper.selectOneTeacherByNumber(teaNUmber);
    }

    @Override
    public List<Teacher> findTeacherList() {
        return teacherMapper.selectTeacherList();
    }

    @Override
    public Teacher selectUserByName(String teaName) {
        return teacherMapper.selectOneTeacherByName(teaName);
    }
}
