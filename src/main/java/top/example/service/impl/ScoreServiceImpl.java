package top.example.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.example.mapper.ScoreMapper;
import top.example.model.Score;
import top.example.service.ScoreService;

import java.util.List;


@Service
public class ScoreServiceImpl implements ScoreService {

    @Autowired
    private ScoreMapper scoreMapper;

    @Override
    public void addScore(Score score) {
        scoreMapper.insertScore(score);
    }

    @Override
    public void deleteScore(String scNumber) {
        scoreMapper.deleteScoreByScNumber(scNumber);
    }

    @Override
    public void updateScore(Score score) {
        scoreMapper.updateScoreInfo(score);
    }

    @Override
    public Score findOneScoreByScNumber(String scNumber) {
        return scoreMapper.selectOneByScNumber(scNumber);
    }

    @Override
    public List<Score> selectListByCommon(Score score) {
        return scoreMapper.selectListByCommon(score);
    }

    @Override
    public List<Score> findScoreListByStuNumber(String stuNumber) {
        return scoreMapper.selectListByStuNumber(stuNumber);
    }

    @Override
    public List<Score> findScoreListByCouNumber(String claNumber) {
        return scoreMapper.selectListByClaNumber(claNumber);
    }


}
