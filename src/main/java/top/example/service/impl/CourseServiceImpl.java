package top.example.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.example.mapper.CourseMapper;
import top.example.model.Course;
import top.example.service.CourseService;

import java.util.List;


@Service
public class CourseServiceImpl implements CourseService {

    @Autowired
    private CourseMapper courseMapper;


    @Override
    public void addCourse(Course courseModel) {
        courseMapper.insetOneCourse(courseModel);
    }

    @Override
    public void deleteCourseByNumber(String scNumber) {
        courseMapper.deleteCourseByNumber(scNumber);
    }

    @Override
    public void updateCourse(Course courseModel) {
        courseMapper.updateCourseInfo(courseModel);
    }

    @Override
    public Course findOneCourseByCouNumber(String stuNumber) {
        return courseMapper.selectOneCourseByNumber(stuNumber);
    }

    @Override
    public List<Course> findList() {
        return courseMapper.selectCourseList();
    }

    @Override
    public List<Course> findListByCouTeaNumber(String couTeaNumber) {
        return courseMapper.selectCourseListByCouTeaNumber(couTeaNumber);
    }
}
