package top.example.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.example.mapper.AdminMapper;
import top.example.model.Admin;
import top.example.service.AdminService;


@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    private AdminMapper adminMapper;


    @Override
    public Admin findOneAdminByNumber(String adminNumber) {
        return this.adminMapper.selectAdminByNumber(adminNumber);
    }

    @Override
    public Admin findOneAdminByName(String adminName) {
        return this.adminMapper.selectUserByName(adminName);
    }

    @Override
    public void updateAdmin(Admin admin) {
        this.adminMapper.updateAdmin(admin);
    }


}
