package top.example.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import top.example.mapper.StudentMapper;
import top.example.model.Student;
import top.example.service.StudentService;

import java.util.List;


@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentMapper studentMapper;


    @Override
    public void addStudent(Student student) {
        studentMapper.insetOneStudent(student);
    }

    @Override
    public void deleteStudent(String studentNUmber) {
        studentMapper.deleteStudentByNumber(studentNUmber);
    }

    @Override
    public void updateStudent(Student student) {
        studentMapper.updateStudentInfo(student);
    }

    @Override
    public Student findStudent(String teaNUmber) {
        return studentMapper.selectOneStudentByNumber(teaNUmber);
    }

    @Override
    public List<Student> findStudentList() {
        return studentMapper.selectStudentList();
    }

    @Override
    public List<Student> findStudentListByCouNumber(String couNumber) {
        return studentMapper.selectStudentListByCouNumber(couNumber);
    }

    @Override
    public List<Student> findStudentListByClaNumber(String claNumber) {
        return studentMapper.selectStudentListByClaNumber(claNumber);
    }

    @Override
    public Student selectUserByName(String teaName) {
        return studentMapper.selectOneStudentByName(teaName);
    }
}
