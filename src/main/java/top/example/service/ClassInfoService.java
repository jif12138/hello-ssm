package top.example.service;

import top.example.model.ClassInfo;

import java.util.List;


public interface ClassInfoService {
    //    void addClass(ClassInfo classModel);
//
//    void deleteClass(String scNumber);
//
    void updateClass(ClassInfo classModel);

    ClassInfo findOneClassByClaNumber(String stuNumber);

    List<ClassInfo> findList();


}
