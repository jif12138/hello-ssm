package top.example.service;


import top.example.model.Score;

import java.util.List;

public interface ScoreService {

    void addScore(Score score);

    void deleteScore(String scNumber);

    void updateScore(Score score);

    Score findOneScoreByScNumber(String stuNumber);

    List<Score> selectListByCommon(Score score);

    List<Score> findScoreListByStuNumber(String stuNumber);

    List<Score> findScoreListByCouNumber(String claNumber);


}