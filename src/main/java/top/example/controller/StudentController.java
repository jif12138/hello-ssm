package top.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import top.example.model.*;
import top.example.service.ScoreService;
import top.example.service.StudentService;
import top.example.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private ScoreService scoreService;
    @Autowired
    private StudentService studentService;

    @Autowired
    private UserService userService;

    @RequestMapping("index")
    public String index(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        String stuNumber = request.getParameter("stuNumber");
        model.addAttribute("currentStu", studentService.findStudent(stuNumber));
        return "Student/index";

    }


    @RequestMapping("showMyScore")
    public String showMyScore(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");

        response.setCharacterEncoding("UTF-8");

        String stuNumber = request.getParameter("stuNumber");

        model.addAttribute("currentStu", studentService.findStudent(stuNumber));
        List<Score> scores = scoreService.findScoreListByStuNumber(stuNumber);
        model.addAttribute("scores", scores);
        return "Student/score_list";

    }

    @RequestMapping("showMyScoreDetail")
    public String showMyScopDetail(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");

        response.setCharacterEncoding("UTF-8");

        String scNumber = request.getParameter("scNumber");
        String stuNumber = request.getParameter("stuNumber");

        model.addAttribute("scoreInfo", scoreService.findOneScoreByScNumber(scNumber));


        model.addAttribute("currentStu", studentService.findStudent(stuNumber));
        return "Student/score_detail";

    }

    @RequestMapping("updateStudent")
    public String updateStudent(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        String stuNumber = request.getParameter("stuNumber");
        String stuName = request.getParameter("stuName");
        String stuPassword = request.getParameter("stuPassword");
        String stuGender = request.getParameter("stuGender");
        String stuMajor = request.getParameter("stuMajor");
        String stuClassNumber = request.getParameter("stuClassNumber");
        String stuTelephone = request.getParameter("stuTelephone");
        if (stuName != null) {
            User user = new User();
            user.setUserNumber(stuNumber);
            user.setUserName(stuName);
            userService.updateUser(user);
        }
        if (stuPassword != null) {
            User user = new User();
            user.setUserNumber(stuNumber);
            user.setUserPassword(stuPassword);
            userService.updateUser(user);
        }
        Student newStu = new Student();
        newStu.setStuName(stuName);
        newStu.setStuNumber(stuNumber);
        newStu.setStuGender(stuGender);
        newStu.setStuMajor(stuMajor);
        newStu.setStuClassNumber(stuClassNumber);
        newStu.setStuTelephone(stuTelephone);
        System.out.println(newStu.toString());
        studentService.updateStudent(newStu);

        List<Student> stucherList = studentService.findStudentList();
        model.addAttribute("studentList", stucherList);


        model.addAttribute("currentStu", studentService.findStudent(stuNumber));

        return "Student/index";
    }
}
