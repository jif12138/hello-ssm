package top.example.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import top.example.model.User;
import top.example.service.AdminService;
import top.example.service.StudentService;
import top.example.service.TeacherService;
import top.example.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private UserService loginService;

    @Autowired
    private TeacherService teacherService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private AdminService adminService;


    @RequestMapping("login")
    public String selectUser(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String userNumber = request.getParameter("usernumber");
        System.out.println("获取到的 userNumber ： " + userNumber);
        User user = this.loginService.checkAccount(userNumber);
        if (user != null) {
            System.out.println("登陆成功");
        } else {
            System.out.println("用户为空，请注册。");
            return "login";
        }

        int userRole = user.getUserRole();
        if (userRole == 0) {
            System.out.println("身份为管理员");
            model.addAttribute("currentAdmin", adminService.findOneAdminByNumber(userNumber));

            return "Admin/index";
        } else if (userRole == 1) {
            System.out.println("身份为教师");
            model.addAttribute("currentTea", teacherService.findTeacher(userNumber));
            return "Teacher/index";
        } else if (userRole == 2) {

            System.out.println("身份为学生");
            model.addAttribute("currentStu", studentService.findStudent(userNumber));
            return "Student/index";

        } else {
            System.out.println("出错");
            return "/login";
        }

    }


    @RequestMapping("logOut")
    public String logOut() {
        return "/login";
    }
}
