package top.example.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import top.example.model.*;
import top.example.service.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


@Controller
@RequestMapping("/admin")
public class AdminController {
    @Autowired
    private AdminService adminService;

    @Autowired
    private TeacherService teacherService;


    @Autowired
    private UserService userService;

    @Autowired
    private StudentService studentService;

    @Autowired
    private ClassInfoService classInfoService;
    @Autowired
    private ScoreService scoreService;
    @Autowired
    private CourseService courseService;

    @RequestMapping("index")
    public String index(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {

        model.addAttribute("currentAdmin", new Admin());
        return "Admin/index";

    }

    /**
     *  教师相关操作
     */

    /**
     * 添加教师
     *
     * @param request
     * @param response
     * @param model
     * @return
     * @throws IOException
     */
    @RequestMapping("addTeacher")
    public String addTeacher(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {

        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String teaNumber = request.getParameter("teaNumber");
        String teaName = request.getParameter("teaName");
        String teaPassword = request.getParameter("teaPassword");

        User user = new User();
        user.setUserName(teaName);
        user.setUserNumber(teaNumber);
        user.setUserRole(1);
        user.setUserPassword(teaPassword);
        System.out.println("获取到的 参数： " + teaNumber + "|" + teaName);
        userService.addUser(user);

        Teacher newTea = new Teacher();
        newTea.setTeaName(teaName);
        newTea.setTeaNumber(teaNumber);
        teacherService.addTeacher(newTea);


        List<Teacher> teacherList = teacherService.findTeacherList();
        model.addAttribute("teacherList", teacherList);
        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));

        return "Admin/tea_list";
    }

    /**
     * 删除 教师
     *
     * @param request
     * @param response
     * @param model
     * @return
     * @throws IOException
     */
    @RequestMapping("deleteTeacher")
    public String deleteTeacher(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        String teaNumber = request.getParameter("teaNumber");

//        删除教师表中的信息
        teacherService.deleteTeacher(teaNumber);
//        删除用户表i中的信息
        userService.deleteUser(teaNumber);

        List<Teacher> teacherList = teacherService.findTeacherList();
        model.addAttribute("teacherList", teacherList);
        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));

        return "Admin/tea_list";
    }

    /**
     * 更新教师信息
     *
     * @param request
     * @param response
     * @param model
     * @return
     * @throws IOException
     */
    @RequestMapping("updateTeacher")
    public String updateTeacher(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        String teaNumber = request.getParameter("teaNumber");
        String teaName = request.getParameter("teaName");
        String teaPassword = request.getParameter("teaPassword");
        if (teaName != null) {
            User user = new User();
            user.setUserNumber(teaNumber);
            user.setUserName(teaName);
            userService.updateUser(user);
        }
        if (teaPassword != null) {
            User user = new User();
            user.setUserNumber(teaNumber);
            user.setUserPassword(teaPassword);
            userService.updateUser(user);
        }
        Teacher newTea = new Teacher();
        newTea.setTeaName(teaName);
        newTea.setTeaNumber(teaNumber);
        teacherService.updateTeacher(newTea);

        List<Teacher> teacherList = teacherService.findTeacherList();
        model.addAttribute("teacherList", teacherList);
        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));

        return "Admin/tea_list";
    }

    /**
     * 展示教师 列表
     *
     * @param request
     * @param response
     * @param model
     * @return
     * @throws IOException
     */
    @RequestMapping("showTeacher")
    public String showTeacher(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        List<Teacher> teacherList = teacherService.findTeacherList();
        model.addAttribute("teacherList", teacherList);
        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));

        return "Admin/tea_list";
    }


    /**
     * 查看教师详细信息
     *
     * @param request
     * @param response
     * @param model
     * @return
     * @throws IOException
     */
    @RequestMapping("showTeacherDetail")
    public String showTeacherDetail(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String teaNumber = request.getParameter("teaNumber");
        System.out.println("查看教师详细信息" + teaNumber);
        Teacher teacher = teacherService.findTeacher(teaNumber);
        model.addAttribute("teacherInfo", teacher);
        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));

        return "Admin/tea_detail";
    }


    /**
     * 学生相关操作
     */


    /**
     * 添加学生
     *
     * @param request
     * @param response
     * @param model
     * @return
     * @throws IOException
     */
    @Transactional
    @RequestMapping("addStudent")
    public String addStudent(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {

        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String stuNumber = request.getParameter("stuNumber");
        String stuName = request.getParameter("stuName");
        String stuPassword = request.getParameter("stuPassword");
        String stuGender = request.getParameter("stuGender");
        String stuMajor = request.getParameter("stuMajor");
        String stuClassNumber = request.getParameter("stuClassNumber");
        String stuTelephone = request.getParameter("stuTelephone");

        User user = new User();
        user.setUserName(stuName);
        user.setUserPassword(stuPassword);
        user.setUserNumber(stuNumber);
        user.setUserRole(2);
        userService.addUser(user);

        Student newStu = new Student();
        newStu.setStuName(stuName);
        newStu.setStuNumber(stuNumber);
        newStu.setStuGender(stuGender);
        newStu.setStuMajor(stuMajor);
        newStu.setStuClassNumber(stuClassNumber);
        newStu.setStuTelephone(stuTelephone);
        studentService.addStudent(newStu);


        List<Student> studentList = studentService.findStudentList();
        model.addAttribute("studentList", studentList);
        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));

        return "Admin/stu_list";
    }

    /**
     * 删除  学生
     *
     * @param request
     * @param response
     * @param model
     * @return
     * @throws IOException
     */
    @RequestMapping("deleteStudent")
    public String deleteStudent(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        String stuNumber = request.getParameter("stuNumber");

//        删除教师表中的信息
        studentService.deleteStudent(stuNumber);
//        删除用户表i中的信息
        userService.deleteUser(stuNumber);

        List<Student> stucherList = studentService.findStudentList();
        model.addAttribute("studentList", stucherList);
        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));

        return "Admin/stu_list";
    }

    /**
     * 更新   学生 信息
     *
     * @param request
     * @param response
     * @param model
     * @return
     * @throws IOException
     */
    @RequestMapping("updateStudent")
    public String updateStudent(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        String stuNumber = request.getParameter("stuNumber");
        String stuName = request.getParameter("stuName");
        String stuPassword = request.getParameter("stuPassword");
        String stuGender = request.getParameter("stuGender");
        String stuMajor = request.getParameter("stuMajor");
        String stuClassNumber = request.getParameter("stuClassNumber");
        String stuTelephone = request.getParameter("stuTelephone");
        if (stuName != null) {
            User user = new User();
            user.setUserNumber(stuNumber);
            user.setUserName(stuName);
            userService.updateUser(user);
        }
        if (stuPassword != null) {
            User user = new User();
            user.setUserNumber(stuNumber);
            user.setUserPassword(stuPassword);
            userService.updateUser(user);
        }
        Student newStu = new Student();
        newStu.setStuName(stuName);
        newStu.setStuNumber(stuNumber);
        newStu.setStuGender(stuGender);
        newStu.setStuMajor(stuMajor);
        newStu.setStuClassNumber(stuClassNumber);
        newStu.setStuTelephone(stuTelephone);
        System.out.println(newStu.toString());
        studentService.updateStudent(newStu);

        List<Student> stucherList = studentService.findStudentList();
        model.addAttribute("studentList", stucherList);
        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));

        return "Admin/stu_list";
    }


    @RequestMapping("showStudent")
    public String showStudent(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        List<Student> studentList = studentService.findStudentList();
        model.addAttribute("studentList", studentList);
        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));

        return "Admin/stu_list";

    }

    @RequestMapping("showStudentDetail")
    public String showStudentDetail(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String stuNumber = request.getParameter("stuNumber");
        System.out.println("查看详细信息" + stuNumber);
        Student stucher = studentService.findStudent(stuNumber);
        model.addAttribute("studentInfo", stucher);
        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));

        return "Admin/stu_detail";
    }

    /**
     * 查看 管理员信息
     *
     * @param request
     * @param response
     * @param model
     * @return
     * @throws IOException
     */
    @RequestMapping("/showSelf")
    public String showSelf(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));

        return "Admin/stu_list";
    }

    /**
     * 查看所有班级  列表
     *
     * @param request
     * @param response
     * @param model
     * @return
     * @throws IOException
     */
    @RequestMapping("showAllClass")
    public String showAllClass(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");


        String type = request.getParameter("type");
        if (type == "score") {
            model.addAttribute("hiddenAddStudent", "none");
        } else {
            model.addAttribute("hiddenAddStudent", "nn");
        }

        List<ClassInfo> classInfos = classInfoService.findList();
        model.addAttribute("classInfos", classInfos);
        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));

        return "Admin/class_list";
    }


    /**
     * 查看班级详细信息
     * 授课教师
     * 课程信息
     * 学生列表
     *
     * @param request
     * @param response
     * @param model
     * @return
     * @throws IOException
     */
    @RequestMapping("/showThisClassDetail")
    public String showThisClassDetail(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String claNumber = request.getParameter("claNumber");
        boolean hiddenAddStudent = Boolean.parseBoolean(request.getParameter("hiddenAddStudent"));
        ClassInfo classInfo = classInfoService.findOneClassByClaNumber(claNumber);
        model.addAttribute("classInfo", classInfo);

        List<Student> studentLis = studentService.findStudentListByClaNumber(claNumber);
        model.addAttribute("students", studentLis);
        model.addAttribute("hiddenAddStudent", hiddenAddStudent);

        List<Score> scores = scoreService.findScoreListByCouNumber(claNumber);
        model.addAttribute("scores", scores);

        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));
        return "Admin/class_detail";
    }

    /**
     * 班级添加学生 时 获取学生列表
     *
     * @param request
     * @param response
     * @param model
     * @return
     * @throws IOException
     */
    @RequestMapping("/showStuInfoList")
    public void showStuInfoList(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        List<Student> addStudentLis = studentService.findStudentListByClaNumber("-1");
        ObjectMapper mapper = new ObjectMapper();
        response.getWriter().write(mapper.writeValueAsString(addStudentLis));
        response.getWriter().close();
    }

    /**
     * 班级添加学生
     */
    @RequestMapping("/addStuToClass")
    public String addStuToClass(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String stuNumber = request.getParameter("stuNumber");
        String claNumber = request.getParameter("claNumber");
        System.out.println(stuNumber + "【】【】】【】【】【" + claNumber);
        Student stu = studentService.findStudent(stuNumber);

        stu.setStuClassNumber(claNumber);
        studentService.updateStudent(stu);

        ClassInfo classInfo = classInfoService.findOneClassByClaNumber(claNumber);
        List<Student> studentLis = studentService.findStudentListByClaNumber(claNumber);

        model.addAttribute("classInfo", classInfo);
        model.addAttribute("students", studentLis);
        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));
        return "Admin/class_detail";
    }

    /**
     * 班级 删除 学生
     */
    @RequestMapping("/deleteStuFromClass")
    public String deleteStuFromClass(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String stuNumber = request.getParameter("stuNumber");
        String claNumber = "-1";
        System.out.println(stuNumber + "【】【】】【】【】【" + claNumber);
        Student s = new Student();
        s.setStuNumber(stuNumber);
        s.setStuClassNumber(claNumber);
        studentService.updateStudent(s);


        ClassInfo classInfo = classInfoService.findOneClassByClaNumber(claNumber);
        List<Student> studentLis = studentService.findStudentListByClaNumber(claNumber);


        model.addAttribute("classInfo", classInfo);
        model.addAttribute("students", studentLis);

        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));

        return "Admin/class_detail";
    }

    /**
     * 班级添加学生
     */
    @RequestMapping("/updateClass")
    public String updateClass(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String claNumber = request.getParameter("claNumber");
        String claName = request.getParameter("claName");

        ClassInfo classInfo = new ClassInfo();
        classInfo.setClaNumber(claNumber);
        classInfo.setClaName(claName);
        classInfoService.updateClass(classInfo);
        List<Student> studentLis = studentService.findStudentListByCouNumber(claNumber);


        model.addAttribute("classInfo", classInfo);
        model.addAttribute("students", studentLis);

        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));

        return "Admin/class_detail";
    }


    /**
     * 成绩管理
     * CRUD
     *
     * @param request
     * @param response
     * @param model
     * @return
     * @throws IOException
     */
    @RequestMapping("/addScore")
    public String addSocre(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String scNumber = request.getParameter("scNumber");
        String scCouNumber = request.getParameter("scCouNumber");
        String scStuNumber = request.getParameter("scStuNumber");
        double scScore = Double.parseDouble(request.getParameter("scScore"));

        Score score = new Score();
        score.setScStuNumber(scStuNumber);
        score.setScNumber(scNumber);
        score.setScCouNumber(scCouNumber);
        score.setScScore(scScore);
        scoreService.addScore(score);

        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));

        List<Score> scores = scoreService.findScoreListByCouNumber(scCouNumber);
        model.addAttribute("scores", scores);
        return "Admin/class_detail";
    }

    @RequestMapping("/deleteScore")
    public String deleteSocre(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String scNumber = request.getParameter("scNumber");
        scoreService.deleteScore(scNumber);

        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));
        List<ClassInfo> classInfos = classInfoService.findList();
        model.addAttribute("classInfos", classInfos);
        return "Admin/class_list";
    }

    @RequestMapping("/updateScore")
    public String updateSocre(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String scNumber = request.getParameter("scNumber");
        String scCouNumber = request.getParameter("scCouNumber");
        String scStuNumber = request.getParameter("scStuNumber");
        double scScore = Double.parseDouble(request.getParameter("scScore"));

        Score score = new Score();
        score.setScStuNumber(scStuNumber);
        score.setScNumber(scNumber);
        score.setScCouNumber(scCouNumber);
        score.setScScore(scScore);

        scoreService.updateScore(score);
        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));


        model.addAttribute("scoreInfo", score);
        return "Admin/score_detail";
    }

    @RequestMapping("/showCourseDetail_ScoreList")
    public String showCourseDetail_ScoreList(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String stuNumber = request.getParameter("stuNumber");
        List<Score> scoreList = scoreService.findScoreListByStuNumber(stuNumber);
        model.addAttribute("scoreList", scoreList);
        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));

        return "Admin/score_list";
    }

    @RequestMapping("/showScoreDetail")
    public String showScoreDetail(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String scNumber = request.getParameter("scNumber");
        Score score = scoreService.findOneScoreByScNumber(scNumber);
        model.addAttribute("scoreInfo", score);
        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));

        return "Admin/score_detail";
    }

    /**
     * 管理员 查看 学生成绩
     *
     * @param request
     * @param response
     * @param model
     * @return
     * @throws IOException
     */
    @RequestMapping("/showStuScore")
    public String showStuScore(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));

        return "Admin/stu_list";
    }

    @RequestMapping("/updateAdmin")
    public String updateAdmin(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        String adminNumber = request.getParameter("adminNumber");
        String adminName = request.getParameter("adminName");

        if (adminName != null) {
            User user = new User();
            user.setUserNumber(adminNumber);
            user.setUserName(adminName);
            userService.updateUser(user);
        }

        Admin newAdmin = new Admin();
        newAdmin.setAdminName(adminName);
        newAdmin.setAdminNumber(adminNumber);

        adminService.updateAdmin(newAdmin);

        List<Student> stucherList = studentService.findStudentList();

        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));

        return "Admin/index";
    }

    /**
     * 课程 管理
     */

    @RequestMapping("/addCourse")
    public String addCourse(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String couNumber = request.getParameter("couNumber");
        String couName = request.getParameter("couName");
        String couTeaNumber = request.getParameter("couTeaNumber");
        String couHour = request.getParameter("couHour");
        String couCredit = request.getParameter("couCredit");
        String couAddress = request.getParameter("couAddress");
        String couMajor = request.getParameter("couMajor");

        Course course = new Course();
        course.setCouAddress(couAddress);
        course.setCouCredit(couCredit);
        course.setCouHour(couHour);
        course.setCouMajor(couMajor);
        course.setCouName(couName);
        course.setCouNumber(couNumber);
        course.setCouTeaNumber(couTeaNumber);
        courseService.addCourse(course);

        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));
        List<Course> courseList = courseService.findList();
        model.addAttribute("courseList", courseList);
        return "Admin/class_detail";
    }

    @RequestMapping("/deleteCourse")
    public String deleteCourse(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String couNumber = request.getParameter("couNumber");
        courseService.deleteCourseByNumber(couNumber);

        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));


        List<Course> courseList = courseService.findList();
        model.addAttribute("courseList", courseList);
        return "Admin/course_list";
    }

    @RequestMapping("/updateCourse")
    public String updateCourse(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        String couNumber = request.getParameter("couNumber");
        String couName = request.getParameter("couName");
        String couTeaNumber = request.getParameter("couTeaNumber");
        String couHour = request.getParameter("couHour");
        String couCredit = request.getParameter("couCredit");
        String couAddress = request.getParameter("couAddress");
        String couMajor = request.getParameter("couMajor");

        Course course = new Course();
        course.setCouAddress(couAddress);
        course.setCouCredit(couCredit);
        course.setCouHour(couHour);
        course.setCouMajor(couMajor);
        course.setCouName(couName);
        course.setCouNumber(couNumber);
        course.setCouTeaNumber(couTeaNumber);

        courseService.updateCourse(course);
        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));
        model.addAttribute("courseInfo", course);
        return "Admin/course_detail";
    }

    /**
     * 查看课程列表
     */
    @RequestMapping("showAllCourse")
    public String showAllCourse(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        List<Course> courseList = courseService.findList();
        model.addAttribute("courseList", courseList);
        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));
        return "Admin/course_list";
    }


    @RequestMapping("/showCourseDetail")
    public String showCourseDetail(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String couNumber = request.getParameter("couNumber");
        Course course = courseService.findOneCourseByCouNumber(couNumber);
        model.addAttribute("courseInfo", course);
        model.addAttribute("currentAdmin", adminService.findOneAdminByNumber("1"));

        return "Admin/course_detail";
    }

}
