package top.example.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import top.example.model.*;
import top.example.service.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/teacher")
public class TeacherController {

    @Autowired
    private UserService userService;
    @Autowired
    private StudentService studentService;
    @Autowired
    private TeacherService teacherService;

    @Autowired
    private ScoreService scoreService;

    @Autowired
    private CourseService courseService;


    @RequestMapping("index")
    public String index(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {


        String teaNumber = request.getParameter("teaNumber");
        model.addAttribute("currentTea", teacherService.findTeacher(teaNumber));
        return "Teacher/index";

    }

    /**
     * 展示自己教授的所有课程
     *
     * @param request
     * @param response
     * @param model
     * @return
     * @throws IOException
     */
    @RequestMapping("showMyClass")
    public String showMyClass(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");

        response.setCharacterEncoding("UTF-8");
        String teaNumber = request.getParameter("teaNumber");


        List<Course> courseInfos = courseService.findListByCouTeaNumber(teaNumber);
        model.addAttribute("CourseInfos", courseInfos);

        model.addAttribute("currentTea", teacherService.findTeacher(teaNumber));
        return "Teacher/course_list";

    }


    /**
     * 选选择 班级 后查看 学生信息
     *
     * @param request
     * @param response
     * @param model
     * @return
     * @throws IOException
     */
    @RequestMapping("showMyClassStuList")
    public String showMyClassStuList(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");

        response.setCharacterEncoding("UTF-8");

        String couNumber = request.getParameter("couNumber");


        System.out.println("【】【【【】【】【");
        System.out.println(couNumber);
        List<Student> studentList = studentService.findStudentListByCouNumber(couNumber);
        model.addAttribute("studentList", studentList);

        String teaNumber = request.getParameter("teaNumber");
        model.addAttribute("currentTea", teacherService.findTeacher(teaNumber));
        return "Teacher/stu_list";
    }

    /**
     * 选择 班级 后 查看成绩 列表
     *
     * @param request
     * @param response
     * @param model
     * @return
     * @throws IOException
     */
    @RequestMapping("showMyClassScoreList")
    public String showMyClassScoreList(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");

        response.setCharacterEncoding("UTF-8");

        String couNumber = request.getParameter("couNumber");


        List<Score> scores = scoreService.findScoreListByCouNumber(couNumber);
        model.addAttribute("scores", scores);
        model.addAttribute("couNumber", couNumber);

        String teaNumber = request.getParameter("teaNumber");
        model.addAttribute("currentTea", teacherService.findTeacher(teaNumber));
        return "Teacher/score_list";
    }


    /**
     *
     */


    @RequestMapping("/showThisClassDetail")
    public String showThisClassDetail(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String claNumber = request.getParameter("claNumber");
        List<Score> scores = scoreService.findScoreListByCouNumber(claNumber);
        model.addAttribute("scores", scores);
        String teaNumber = request.getParameter("teaNumber");
        model.addAttribute("currentTea", teacherService.findTeacher(teaNumber));
        return "Teacher/class_detail";
    }

    /**
     * 添加学生成绩信息
     *
     * @param request
     * @param response
     * @param model
     * @return
     * @throws IOException
     */
    @RequestMapping("/addScore")
    public String addSocre(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String scNumber = request.getParameter("scNumber");
        String scCouNumber = request.getParameter("scCouNumber");
        String scStuNumber = request.getParameter("scStuNumber");
        double scScore = Double.parseDouble(request.getParameter("scScore"));

        Score score = new Score();
        score.setScStuNumber(scStuNumber);
        score.setScNumber(scNumber);
        score.setScCouNumber(scCouNumber);
        score.setScScore(scScore);
        scoreService.addScore(score);

        String teaNumber = request.getParameter("teaNumber");
        model.addAttribute("currentTea", teacherService.findTeacher(teaNumber));


        List<Score> scores = scoreService.findScoreListByCouNumber(scCouNumber);
        model.addAttribute("scores", scores);
        return "Teacher/score_list";
    }

    /**
     * 删除学生成绩信息
     *
     * @param request
     * @param response
     * @param model
     * @return
     * @throws IOException
     */
    @RequestMapping("/deleteScore")
    public String deleteScore(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String scNumber = request.getParameter("scNumber");
        String scCouNumber = request.getParameter("scCouNumber");
        scoreService.deleteScore(scNumber);

        String teaNumber = request.getParameter("teaNumber");
        List<Score> scores = scoreService.findScoreListByCouNumber(scCouNumber);
        model.addAttribute("scores", scores);
        model.addAttribute("currentTea", teacherService.findTeacher(teaNumber));
        return "Teacher/score_list";
    }

    /**
     * 更新学生成绩信息
     *
     * @param request
     * @param response
     * @param model
     * @return
     * @throws IOException
     */
    @RequestMapping("/updateScore")
    public String updateSocre(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String scNumber = request.getParameter("scNumber");
        String scCouNumber = request.getParameter("scCouNumber");
        String scStuNumber = request.getParameter("scStuNumber");
        double scScore = Double.parseDouble(request.getParameter("scScore"));
        String teaNumber = request.getParameter("teaNumber");

        Score score = new Score();
        score.setScStuNumber(scStuNumber);
        score.setScNumber(scNumber);
        score.setScCouNumber(scCouNumber);
        score.setScScore(scScore);

        scoreService.updateScore(score);


        model.addAttribute("scoreInfo", score);

        System.out.println("更新 Score 后传回的teacnumber:::::::");
        System.out.println(teaNumber);
        model.addAttribute("currentTea", teacherService.findTeacher(teaNumber));
        return "Teacher/score_detail";
    }

    /**
     * 展示成绩详情
     *
     * @param request
     * @param response
     * @param model
     * @return
     * @throws IOException
     */
    @RequestMapping("/showScoreDetail")
    public String showScoreDetail(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");

        String scNumber = request.getParameter("scNumber");
        Score score = scoreService.findOneScoreByScNumber(scNumber);
        model.addAttribute("scoreInfo", score);
        String teaNumber = request.getParameter("teaNumber");
        model.addAttribute("currentTea", teacherService.findTeacher(teaNumber));
        return "Teacher/score_detail";
    }

    @RequestMapping("updateTeacher")
    public String updateTeacher(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        String teaNumber = request.getParameter("teaNumber");
        String teaName = request.getParameter("teaName");
        String teaPassword = request.getParameter("teaPassword");
        if (teaName != null) {
            User user = new User();
            user.setUserNumber(teaNumber);
            user.setUserName(teaName);
            userService.updateUser(user);
        }
        if (teaPassword != null) {
            User user = new User();
            user.setUserNumber(teaNumber);
            user.setUserPassword(teaPassword);
            userService.updateUser(user);
        }
        Teacher newTea = new Teacher();
        newTea.setTeaName(teaName);
        newTea.setTeaNumber(teaNumber);
        teacherService.updateTeacher(newTea);

        List<Teacher> teacherList = teacherService.findTeacherList();
        model.addAttribute("teacherList", teacherList);
        model.addAttribute("currentTea", teacherService.findTeacher(teaNumber));

        return "Teacher/index";
    }

    @RequestMapping("updateStudent")
    public String updateStudent(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        String stuNumber = request.getParameter("stuNumber");
        String teaNumber = request.getParameter("teaNumber");
        String stuName = request.getParameter("stuName");
        String stuPassword = request.getParameter("stuPassword");
        String stuGender = request.getParameter("stuGender");
        String stuMajor = request.getParameter("stuMajor");
        String stuClassNumber = request.getParameter("stuClassNumber");
        String stuTelephone = request.getParameter("stuTelephone");
        if (stuName != null) {
            User user = new User();
            user.setUserNumber(stuNumber);
            user.setUserName(stuName);
            userService.updateUser(user);
        }
        if (stuPassword != null) {
            User user = new User();
            user.setUserNumber(stuNumber);
            user.setUserPassword(stuPassword);
            userService.updateUser(user);
        }
        Student newStu = new Student();
        newStu.setStuName(stuName);
        newStu.setStuNumber(stuNumber);
        newStu.setStuGender(stuGender);
        newStu.setStuMajor(stuMajor);
        newStu.setStuClassNumber(stuClassNumber);
        newStu.setStuTelephone(stuTelephone);
        System.out.println(newStu.toString());
        studentService.updateStudent(newStu);

        List<Student> stucherList = studentService.findStudentList();
        model.addAttribute("studentList", stucherList);
        model.addAttribute("currentTea", teacherService.findTeacher(teaNumber));

        return "Teacher/stu_detail";
    }

    @RequestMapping("showStudentDetail")
    public String showStudentDetail(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        String teaNUmber = request.getParameter("teaNumber");
        String stuNumber = request.getParameter("stuNumber");
        System.out.println("查看详细信息" + stuNumber);
        Student stucher = studentService.findStudent(stuNumber);
        model.addAttribute("studentInfo", stucher);

        model.addAttribute("currentTea", teacherService.findTeacher(teaNUmber));
        return "Teacher/stu_detail";
    }
}
