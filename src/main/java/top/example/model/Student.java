package top.example.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class Student {

    @Id
    private String stuNumber;
    private String stuName;
    private String stuGender;
    private String stuMajor;
    private String stuClassNumber;
    private String stuTelephone;

}
