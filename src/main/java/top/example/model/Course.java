package top.example.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class Course {

    @Id
    private String couNumber;
    private String couName;
    private String couTeaNumber;
    private String couHour;
    private String couCredit;
    private String couAddress;
    private String couMajor;

}
