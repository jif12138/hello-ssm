package top.example.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class User {

    @Id
    private String userNumber;
    private String userName;
    private String userPassword;
    private int userRole;

}
