package top.example.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class Score {

    @Id
    private String scNumber;
    private String scCouNumber;
    private String scStuNumber;
    private double scScore;

}
