package top.example.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class Teacher {

    @Id
    private String teaNumber;
    private String teaName;

}
