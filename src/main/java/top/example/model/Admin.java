package top.example.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class Admin {

    @Id
    private String adminNumber;
    private String adminName;

}
