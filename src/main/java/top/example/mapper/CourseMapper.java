package top.example.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.example.model.Course;

import java.util.List;

@Mapper
public interface CourseMapper {
    void insetOneCourse(Course course);

    void deleteCourseByNumber(String courseNumber);

    void updateCourseInfo(Course course);


    Course selectOneCourseByNumber(String teaNumber);

    Course selectOneCourseByName(String teaName);

    List<Course> selectCourseList();

    List<Course> selectCourseListByCouTeaNumber(String couTeaNumber);
}
