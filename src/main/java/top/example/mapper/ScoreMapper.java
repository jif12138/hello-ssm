package top.example.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.example.model.Score;

import java.util.List;

@Mapper
public interface ScoreMapper {
    void insertScore(Score score);

    void deleteScoreByScNumber(String scNumber);

    void updateScoreInfo(Score score);

    Score selectOneByScNumber(String scNumber);


    List<Score> selectListByClaNumber(String scCouNumber);

    List<Score> selectListByStuNumber(String scStuNumber);

    List<Score> selectListByCommon(Score score);
}

