package top.example.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.example.model.Admin;

@Mapper
public interface AdminMapper {

    Admin selectAdminByNumber(String adminNumber);

    Admin selectUserByName(String adminName);

    void updateAdmin(Admin admin);
}
