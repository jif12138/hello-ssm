package top.example.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.example.model.ClassInfo;

import java.util.List;


@Mapper
public interface ClassInfoMapper {

    void updateClassInfo(ClassInfo classInfo);

    ClassInfo selectOneClassByNumber(String claNumber);


    List<ClassInfo> selectList();


}
