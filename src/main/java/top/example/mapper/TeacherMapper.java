package top.example.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.example.model.Teacher;

import java.util.List;

@Mapper
public interface TeacherMapper {
    void insetOneTeacher(Teacher teacher);

    void deleteTeacherByNumber(String teacherNumber);

    void updateTeacherInfo(Teacher teacher);


    List<Teacher> selectTeacherList();

    Teacher selectOneTeacherByNumber(String teaNumber);

    Teacher selectOneTeacherByName(String teaName);


}
