package top.example.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.example.model.Student;

import java.util.List;

@Mapper
public interface StudentMapper {
    void insetOneStudent(Student student);

    void deleteStudentByNumber(String studentNUmber);

    void updateStudentInfo(Student student);

    Student selectOneStudentByNumber(String stuNumber);

    Student selectOneStudentByName(String stuName);

    List<Student> selectStudentList();

    List<Student> selectStudentListByCouNumber(String stuClassNumber);

    List<Student> selectStudentListByClaNumber(String stuClassNumber);


}
