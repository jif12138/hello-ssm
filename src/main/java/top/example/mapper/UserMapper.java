package top.example.mapper;

import org.apache.ibatis.annotations.Mapper;
import top.example.model.User;


@Mapper
public interface UserMapper {

    void insertUser(User user);


    User selectUser(String userNumber);

    User selectUserByName(String username);

    void deleteUserByNumber(String userNumber);

    void updateUser(User user);
}
