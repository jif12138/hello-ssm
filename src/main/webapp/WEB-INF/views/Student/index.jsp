<!DOCTYPE html>
<html>
<head>
    <%@page language="java" import="java.util.*" pageEncoding="UTF-8" %>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- 全局js -->
    <script src="../statics/js/jquery.min.js?v=2.1.4"></script>
    <link href="../statics/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="../statics/css/font-awesome.css?v=4.4.0" rel="stylesheet">
    <link href="../statics/css/animate.css" rel="stylesheet">
    <link href="../statics/css/style.css?v=4.1.0" rel="stylesheet">


    <script src="../statics/js/bootstrap.min.js?v=3.3.6"></script>


    <!-- 自定义js -->
    <script src="../statics/js/content.js?v=1.0.0"></script>


    <!-- Flot -->
    <script src="../statics/js/plugins/flot/jquery.flot.js"></script>
    <script src="../statics/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="../statics/js/plugins/flot/jquery.flot.resize.js"></script>

    <!-- ChartJS-->
    <script src="../statics/js/plugins/chartJs/Chart.min.js"></script>

    <!-- Peity -->
    <script src="../statics/js/plugins/peity/jquery.peity.min.js"></script>

    <!-- Peity demo -->
    <script src="../statics/js/demo/peity-demo.js"></script>

    <title>学生首页</title>
</head>

<body class="gray-bg top-navigation">

<div id="wrapper">

    <div id="page-wrapper" class="gray-bg">

        <!-- 顶部导航栏 -->
        <div class="row border-bottom white-bg">
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-header">
                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse"
                            class="navbar-toggle collapsed" type="button">
                        <i class="fa fa-reorder"></i>
                    </button>
                    <a href="#" class="navbar-brand">${currentStu.stuName}</a>
                </div>
                <div class="navbar-collapse collapse" id="navbar">
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a aria-expanded="false" role="button"
                               href="${contextPath}/student/index?stuNumber=${currentStu.stuNumber}"> 返回首页</a>
                        </li>


                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 成绩管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li>
                                    <a href="${contextPath}/student/showMyScore?stuNumber=${currentStu.stuNumber}">查询成绩</a>
                                </li>
                            </ul>
                        </li>


                    </ul>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a href="${contextPath}/login/logOut">
                                <i class="fa fa-sign-out"></i> 注销
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>


        <div class="wrapper wrapper-content">
            <div class="row animated fadeInRight">
                <div class="col-sm-4">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>个人资料</h5>
                        </div>
                        <div>
                            <div class="ibox-content no-padding border-left-right">
                                <img alt="image" class="img-responsive" src="../statics/img/profile_big.jpg">
                            </div>
                            <div class="ibox-content profile-content">
                                <h4><strong>${currentStu.stuName} </strong></h4>
                                <p><i class="fa fa-map-marker"></i> 上海市闵行区绿地科技岛广场A座2606室</p>
                                <h5>
                                    关于我
                                </h5>
                                <p>编号： ${currentStu.stuNumber} </p>
                                <p>姓名： ${currentStu.stuName} </p>
                                <p>性别： ${currentStu.stuGender} </p>
                                <p>专业： ${currentStu.stuMajor} </p>
                                <p>学校： ${currentStu.stuClassNumber} </p>
                                <p>电话： ${currentStu.stuTelephone} </p>
                                </p>
                                <div class="row m-t-lg">
                                    <div class="col-sm-4">
                                        <span class="bar">5,3,9,6,5,9,7,3,5,2</span>
                                        <h5><strong>169</strong> 文章</h5>
                                    </div>
                                    <div class="col-sm-4">
                                        <span class="line">5,3,9,6,5,9,7,3,5,2</span>
                                        <h5><strong>28</strong> 关注</h5>
                                    </div>
                                    <div class="col-sm-4">
                                        <span class="bar">5,3,2,-1,-3,-2,2,3,5,2</span>
                                        <h5><strong>240</strong> 关注者</h5>
                                    </div>
                                </div>
                                <div class="user-button">
                                    <div class="row">

                                        <div class="col-sm-6">
                                            <button type="button" class="btn btn-primary btn-sm btn-block">
                                                <i class="fa fa-envelope showEditTable"> 修改信息</i>
                                            </button>
                                        </div>
                                        <div class="col-sm-6">
                                            <button type="button"
                                                    class=" showEditTable btn btn-primary btn-sm btn-block"><i
                                                    class="fa fa-envelope "> 修改信息</i>
                                            </button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>最新动态</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="profile.html#">
                                    <i class="fa fa-wrench"></i>
                                </a>

                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">

                            <div>
                                <div class="feed-activity-list">

                                    <div class="feed-element">
                                        <a href="profile.html#" class="pull-left">
                                            <img alt="image" class="img-circle" src="../statics/img/a1.jpg">
                                        </a>
                                        <div class="media-body ">
                                            <small class="pull-right text-navy">1天前</small>
                                            <strong>奔波儿灞</strong> 关注了 <strong>灞波儿奔</strong>.
                                            <br>
                                            <small class="text-muted">54分钟前 来自 皮皮时光机</small>
                                            <div class="actions">
                                                <a class="btn btn-xs btn-white"><i class="fa fa-thumbs-up"></i> 赞 </a>
                                                <a class="btn btn-xs btn-danger"><i class="fa fa-heart"></i> 收藏</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="feed-element">
                                        <a href="profile.html#" class="pull-left">
                                            <img alt="image" class="img-circle" src="../statics/img/profile.jpg">
                                        </a>
                                        <div class="media-body ">
                                            <small class="pull-right">5分钟前</small>
                                            <strong>作家崔成浩</strong> 发布了一篇文章
                                            <br>
                                            <small class="text-muted">今天 10:20 来自 iPhone 6 Plus</small>

                                        </div>
                                    </div>

                                    <div class="feed-element">
                                        <a href="profile.html#" class="pull-left">
                                            <img alt="image" class="img-circle" src="../statics/img/a2.jpg">
                                        </a>
                                        <div class="media-body ">
                                            <small class="pull-right">2小时前</small>
                                            <strong>作家崔成浩</strong> 抽奖中了20万
                                            <br>
                                            <small class="text-muted">今天 09:27 来自 Koryolink iPhone</small>
                                            <div class="well">
                                                抽奖，人民币2000元，从转发这个微博的粉丝中抽取一人。11月16日平台开奖。随手一转，万一中了呢？
                                            </div>
                                            <div class="pull-right">
                                                <a class="btn btn-xs btn-white"><i class="fa fa-thumbs-up"></i> 赞 </a>
                                                <a class="btn btn-xs btn-white"><i class="fa fa-heart"></i> 收藏</a>
                                                <a class="btn btn-xs btn-primary"><i class="fa fa-pencil"></i> 评论</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="feed-element">
                                        <a href="profile.html#" class="pull-left">
                                            <img alt="image" class="img-circle" src="../statics/img/a3.jpg">
                                        </a>
                                        <div class="media-body ">
                                            <small class="pull-right">2天前</small>
                                            <strong>天猫</strong> 上传了2张图片
                                            <br>
                                            <small class="text-muted">11月7日 11:56 来自 微博 weibo.com</small>
                                            <div class="photos">
                                                <a target="_blank"
                                                   href="http://24.media.tumblr.com/20a9c501846f50c1271210639987000f/tumblr_n4vje69pJm1st5lhmo1_1280.jpg">
                                                    <img alt="image" class="feed-photo" src="../statics/img/p1.jpg">
                                                </a>
                                                <a target="_blank"
                                                   href="http://37.media.tumblr.com/9afe602b3e624aff6681b0b51f5a062b/tumblr_n4ef69szs71st5lhmo1_1280.jpg">
                                                    <img alt="image" class="feed-photo" src="../statics/img/p3.jpg">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="feed-element">
                                        <a href="profile.html#" class="pull-left">
                                            <img alt="image" class="img-circle" src="../statics/img/a4.jpg">
                                        </a>
                                        <div class="media-body ">
                                            <small class="pull-right text-navy">5小时前</small>
                                            <strong>在水一方Y</strong> 关注了 <strong>那二十年的单身</strong>.
                                            <br>
                                            <small class="text-muted">今天 10:39 来自 iPhone客户端</small>
                                            <div class="actions">
                                                <a class="btn btn-xs btn-white"><i class="fa fa-thumbs-up"></i> 赞 </a>
                                                <a class="btn btn-xs btn-white"><i class="fa fa-heart"></i> 收藏</a>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <button class="btn btn-primary btn-block m"><i class="fa fa-arrow-down"></i> 显示更多
                                </button>

                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="footer">
            <div class="pull-right">
                By：<a href="http://www.zi-han.net" target="_blank">zihan's blog</a>
            </div>
            <div>
                <strong>Copyright</strong> H+ &copy; 2014
            </div>
        </div>


        <!--修改表单-->
        <div class="row editTable">
            <div class="col-1"></div>
            <div class="col-md-10 ">
                <h1>修改信息</h1>
                <form action="${contextPath}/student/updateStudent?stuNumber=${currentStu.stuNumber}" method="post">
                    <div class="form-groups">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">成绩编号</label>
                            <div class="col-sm-10">
                                <input type="text" disabled="" value="${currentStu.stuNumber}"
                                       class="form-control" name="stuNumber">
                                <input type="text" hidden value="${currentStu.stuNumber}"
                                       class="form-control" name="stuNumber">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">课程编号</label>
                            <div class="col-sm-10">
                                <input type="text" value="${currentStu.stuName}"
                                       class="form-control" name="stuName">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">学生编号</label>
                            <div class="col-sm-10">
                                <input type="text" value="${currentStu.stuGender}"
                                       class="form-control" name="stuGender">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">成绩</label>
                            <div class="col-sm-10">
                                <input type="text" value="${currentStu.stuMajor}"
                                       class="form-control" name="stuMajor">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">成绩</label>
                            <div class="col-sm-10">
                                <input type="text" value="${currentStu.stuSchool}"
                                       class="form-control" name="stuSchool">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">成绩</label>
                            <div class="col-sm-10">
                                <input type="text" value="${currentStu.stuTelephone}"
                                       class="form-control" name="stuTelephone">
                            </div>
                        </div>

                    </div>
                    <div class="row table11">
                        <button class="btn btn-w-m btn-danger" type="submit">确认修改</button>
                        <button class="btn btn-w-m btn-success hideEditTable" type="button">取消修改</button>
                    </div>

                </form>
            </div>
            <div class="col-1"></div>
        </div>


        <div class="row editTable">
            <div class="col-1"></div>
            <div class="col-md-10 ">
                <h1>修改信息</h1>
                <form action="${contextPath}/admin/updateAdmin" method="post">
                    <div class="form-groups">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">编号</label>
                            <div class="col-sm-10">
                                <input type="text" value="${currentAdmin.adminNumber}"
                                       name="adminNumber" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">姓名</label>
                            <div class="col-sm-10">
                                <input type="text" value="${currentAdmin.adminName}"
                                       name="adminName" class="form-control">
                            </div>
                        </div>

                    </div>
                    <div class="table1111" style="height: 10px;"></div>
                    <div class="row table11">
                        <button class="btn btn-w-m btn-danger" type="submit">确认修改</button>
                        <button class="btn btn-w-m btn-success hideEditTable" type="button">取消修改</button>
                    </div>
                </form>
            </div>
            <div class="col-1"></div>
        </div>

        <!--修改教师信息-->
        <div class="row editTable">
            <div class="col-1"></div>
            <div class="col-md-10 ">
                <h1>修改信息</h1>
                <form action="${contextPath}/teacher/updateTeacher" method="post">
                    <div class="form-groups">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">教师编号</label>
                            <div class="col-sm-10">
                                <input type="text" value="${currentTea.teaNumber}"
                                       name="teaNumber" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">教师姓名</label>
                            <div class="col-sm-10">
                                <input type="text" value="${currentTea.teaName}"
                                       name="teaName" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="table1111" style="height: 10px;"></div>
                    <div class="row table11">
                        <button class="btn btn-w-m btn-danger" type="submit">确认修改</button>
                        <button class="btn btn-w-m btn-success hideEditTable" type="button">取消修改</button>
                    </div>
                </form>
            </div>
            <div class="col-1"></div>
        </div>
    </div>
</div>


<style>
    .editTable {
        width: 800px;
        height: 500px;
        background-color: gainsboro;
        display: none;
        position: absolute;

        border-radius: 20px;
        margin: auto;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }


    .table1111 {
        weight: 800px;
        height: 100px;

    }

    .table11 {
        margin-top: 200px;
        display: flex;
        flex-direction: row;
        justify-content: space-between;

    }

    .form-groups {
        margin-left: 80px;
    }

    .ttt1 {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
    }

</style>
<script>

    $(document).ready(function () {

        $(".showEditTable").click(function () {
            console.log("正确执行到了显示方法！")
            $(".editTable").fadeIn();
        });
        $(".hideEditTable").click(function () {
            $(".editTable").fadeOut();
        });
    });

</script>


<script type="text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>
<!--统计代码，可删除-->

</body>

</html>
