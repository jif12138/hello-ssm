<html>
<head>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <script src="../statics/js/jquery.min.js?v=2.1.4"></script>

    <link href="../statics/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="../statics/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="../statics/css/animate.css" rel="stylesheet">
    <link href="../statics/css/style.css?v=4.1.0" rel="stylesheet">

    <!-- 全局js -->

    <script src="../statics/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="../statics/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../statics/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../statics/js/plugins/layer/layer.min.js"></script>

    <!-- 自定义js -->
    <script src="../statics/js/hplus.js?v=4.1.0"></script>
    <script type="text/javascript" src="../statics/js/contabs.js"></script>

    <!-- 第三方插件 -->
    <!-- jQuery Validation plugin javascript-->
    <script src="../statics/js/plugins/validate/jquery.validate.min.js"></script>
    <script src="../statics/js/plugins/validate/messages_zh.min.js"></script>
    <script src="../statics/js/demo/form-validate-demo.js"></script>

    <title>教授学生列表</title>
</head>
<body class="gray-bg top-navigation">

<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
        <!-- 顶部导航栏 -->
        <div class="row border-bottom white-bg">
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-header">
                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse"
                            class="navbar-toggle collapsed" type="button">
                        <i class="fa fa-reorder"></i>
                    </button>
                    <a href="#" class="navbar-brand">${currentStu.stuName}</a>
                </div>
                <div class="navbar-collapse collapse" id="navbar">
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a aria-expanded="false" role="button"
                               href="${contextPath}/student/index?stuNumber=${currentStu.stuNumber}"> 返回首页</a>
                        </li>


                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 成绩管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li>
                                    <a href="${contextPath}/student/showMyScore?stuNumber=${currentStu.stuNumber}">查询成绩</a>
                                </li>
                            </ul>
                        </li>


                    </ul>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a href="${contextPath}/login/logOut">
                                <i class="fa fa-sign-out"></i> 注销
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>

        <!-- 中部内容 -->
        <div class="wrapper wrapper-content">
            <div class="container">
                <div class="container">
                    <div class="wrapper wrapper-content">
                        <div class="col-md-4"></div>

                        <div class="col-md-4">
                            <div class="widget-head-color-box navy-bg p-lg text-center">
                                <div class="m-b-md">

                                    <h2 class="font-bold no-margins">
                                        成绩详情
                                    </h2>
                                    <small></small>
                                </div>
                            </div>
                            <div class="widget-text-box">
                                <h4 class="media-heading">
                                    成绩编号：【${scoreInfo.scNumber}】
                                </h4>

                                成绩编号：【 <span>${scoreInfo.scNumber}</span> 】
                                <br>
                                课程编号：【<span> ${scoreInfo.scCouNumber} </span>】
                                <br>
                                学生编号：【<span> ${scoreInfo.scStuNumber} </span>】
                                <br>
                                成绩：【<span> ${scoreInfo.scScore} </span>】
                                <br>

                                <div class="text-right">
                                    <a class="btn btn-xs btn-primary showEditTable"><i class="fa fa-edit"></i> 修改</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4"></div>
                    </div>


                </div>
            </div>


            <!-- 底部 -->
            <div class="footer">
                <div class="pull-right">
                    By：<a href="http://www.zi-han.net" target="_blank">zihan's blog</a>
                </div>
                <div>
                    <strong>Copyright</strong> H+ &copy; 2014
                </div>
            </div>


        </div>
    </div>
</div>
</body>

</html>
