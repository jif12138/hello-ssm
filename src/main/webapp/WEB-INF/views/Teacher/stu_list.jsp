<html>
<head>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

    <link href="../statics/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="../statics/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="../statics/css/animate.css" rel="stylesheet">
    <link href="../statics/css/style.css?v=4.1.0" rel="stylesheet">


    <!-- 全局js -->
    <script src="../statics/js/jquery.min.js?v=2.1.4"></script>
    <script src="../statics/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="../statics/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../statics/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../statics/js/plugins/layer/layer.min.js"></script>

    <!-- 自定义js -->
    <script src="../statics/js/hplus.js?v=4.1.0"></script>
    <script type="text/javascript" src="../statics/js/contabs.js"></script>

    <!-- 第三方插件 -->
    <script src="../statics/js/plugins/pace/pace.min.js"></script>

    <title>学生列表</title>
</head>
<body class="gray-bg top-navigation">

<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
        <!-- 顶部导航栏 -->
        <div class="row border-bottom white-bg">
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-header">
                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse"
                            class="navbar-toggle collapsed" type="button">
                        <i class="fa fa-reorder"></i>
                    </button>
                    <a href="#" class="navbar-brand">${currentTea.teaName}</a>
                </div>
                <div class="navbar-collapse collapse" id="navbar">
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a aria-expanded="false" role="button"
                               href="${contextPath}/teacher/index?teaNumber=${currentTea.teaNumber}"> 返回首页</a>
                        </li>
                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 用户管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li>
                                    <a href="${contextPath}/teacher/showMyClass?teaNumber=${currentTea.teaNumber}">学生管理</a>
                                </li>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 成绩管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li><a href="${contextPath}/teacher/showMyClass?teaNumber=${currentTea.teaNumber}">查看授课班级列表</a>
                                </li>

                            </ul>
                        </li>


                    </ul>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a href="${contextPath}/login/logOut">
                                <i class="fa fa-sign-out"></i> 注销
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>


        <!-- 中部内容 -->
        <div class="wrapper wrapper-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>学生列表</h5>

                            </div>
                            <div class="ibox-content">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>编号</th>
                                        <th>姓名</th>
                                        <th>性别</th>
                                        <th>专业</th>
                                        <th>班级</th>
                                        <th>电话</th>
                                        <th>操作</th>


                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${studentList}" var="oneStu">
                                        <tr>
                                            <td>${oneStu.stuNumber}</td>
                                            <td> ${oneStu.stuName}</td>
                                            <td> ${oneStu.stuGender}</td>
                                            <td> ${oneStu.stuMajor}</td>
                                            <td> ${oneStu.stuClassNumber}</td>
                                            <td> ${oneStu.stuTelephone}</td>


                                            <td>
                                                <a href="${contextPath}/teacher/showStudentDetail?stuNumber=${oneStu.stuNumber}&teaNumber=${currentTea.teaNumber}">
                                                    详情</a>
                                            </td>

                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2"></div>
                </div>
            </div>
        </div>


        <!-- 底部 -->
        <div class="footer">
            <div class="pull-right">
                By：<a href="http://www.zi-han.net" target="_blank">zihan's blog</a>
            </div>
            <div>
                <strong>Copyright</strong> H+ &copy; 2014
            </div>
        </div>

    </div>
</div>
<style>
    .addTable {
        width: 800px;
        height: 350px;
        background-color: gainsboro;
        display: none;
        position: absolute;

        border-radius: 20px;
        margin: auto;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }

</style>

<script>

    $(document).ready(function () {

        $(".showAdd").click(function () {
            $(".addTable").fadeIn();
        });
        $(".hideAdd").click(function () {
            $(".addTable").fadeOut();
        });
    });
</script>


</body>

</html>
