<html>
<head>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

    <link href="../statics/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="../statics/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="../statics/css/animate.css" rel="stylesheet">
    <link href="../statics/css/style.css?v=4.1.0" rel="stylesheet">

    <!-- 全局js -->

    <script src="../statics/js/jquery.min.js?v=2.1.4"></script>
    <script src="../statics/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="../statics/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../statics/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../statics/js/plugins/layer/layer.min.js"></script>

    <!-- 自定义js -->
    <script src="../statics/js/hplus.js?v=4.1.0"></script>
    <script type="text/javascript" src="../statics/js/contabs.js"></script>

    <!-- 第三方插件 -->
    <!-- jQuery Validation plugin javascript-->
    <script src="../statics/js/plugins/validate/jquery.validate.min.js"></script>
    <script src="../statics/js/plugins/validate/messages_zh.min.js"></script>
    <script src="../statics/js/demo/form-validate-demo.js"></script>

    <title>班级详情</title>
</head>
<body class="gray-bg top-navigation">

<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">

        <!-- 顶部导航栏 -->
        <div class="row border-bottom white-bg">
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-header">
                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse"
                            class="navbar-toggle collapsed" type="button">
                        <i class="fa fa-reorder"></i>
                    </button>
                    <a href="#" class="navbar-brand">${currentAdmin.adminName}</a>
                </div>
                <div class="navbar-collapse collapse" id="navbar">
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a aria-expanded="false" role="button"
                               href="${contextPath}/admin/index?userNumber=${currentAdmin.adminNumber}"> 返回首页</a>
                        </li>

                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 用户管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li><a href="${contextPath}/admin/showTeacher">教师用户管理</a>
                                </li>
                                <li><a href="${contextPath}/admin/showStudent">学生用户管理</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 班级管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li>
                                    <a href="${contextPath}/admin/showAllClass?type=class">查看班级列表</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 课程管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li>
                                    <a href="${contextPath}/admin/showAllCourse">查看课程列表</a>
                                </li>

                            </ul>
                        </li>

                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 成绩管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li>
                                    <a href="${contextPath}/admin/showAllClass?type=score">查看班级列表</a>
                                </li>

                            </ul>
                        </li>

                    </ul>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a href="${contextPath}/login/logOut">
                                <i class="fa fa-sign-out"></i> 注销
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>


        <!-- 中部内容 -->
        <div class="wrapper wrapper-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <div class="ibox float-e-margins">
                            <h1> ${classInfo.claName}</h1>
                            <h4> ${classInfo.claNumber}</h4>

                            <div class="ibox-title">
                                <h5>班级学生列表</h5>
                                <hr>
                                <div class="classOrScore" style="display: ${hiddenAddStudent}">
                                    <a class="btn btn-primary showAdd" onclick="addStuToClass()"><i
                                            class="glyphicon glyphicon-plus"></i>&nbsp;
                                        添加学生
                                    </a>
                                    <a class="btn btn-primary showEditTable"><i
                                            class="glyphicon glyphicon-plus"></i>&nbsp;
                                        修改课程信息
                                    </a>
                                </div>

                            </div>
                            <div class="ibox-content">
                                <table class="table table-hover">
                                    <thead> `
                                    <tr>
                                        <th>学生编号</th>
                                        <th>学生姓名</th>
                                        <th>性别</th>
                                        <th>专业</th>
                                        <th>电话</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${students}" var="a">
                                        <tr>

                                            <td>${a.stuNumber}</td>
                                            <td>${a.stuName}</td>

                                            <td>${a.stuGender}</td>
                                            <td>${a.stuMajor}</td>

                                            <td>${a.stuTelephone}</td>

                                            <td>
                                                <a href="${contextPath}/admin/showCourseDetail_ScoreList?stuNumber=${a.stuNumber}">
                                                    详情</a>
                                            </td>
                                            <td>
                                                <a href="${contextPath}/admin/deleteStuFromClass?stuNumber=${a.stuNumber}">
                                                    删除</a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>
        </div>


        <!-- 底部 -->
        <div class="footer">
            <div class="pull-right">
                By：<a href="http://www.zi-han.net" target="_blank">zihan's blog</a>
            </div>
            <div>
                <strong>Copyright</strong> H+ &copy; 2014
            </div>
        </div>
        <%-- 添加表单2--%>
        <div class="ibox-content  addTable">
            <form class="form-horizontal m-t" id="signupForm" action="${contextPath}/admin/addStuToClass" method="post">

                <div class="form-group addStuToClassInput">
                    <input type="text" name="claNumber" value="${classInfo.claNumber}" hidden="hidden">
                </div>


                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                        <button class="btn btn-primary btn-danger" type="submit">提交</button>
                        <button class="btn btn-primary  btn-success hideAdd" type="button"> 取消</button>

                    </div>
                </div>
            </form>
        </div>

        <!--修改课程信息-->
        <div class="row editTable">
            <div class="col-1"></div>
            <div class="col-md-10 ">
                <h1>修改课程信息</h1>
                <form action="${contextPath}/admin/updateClass" method="post">
                    <div class="form-groups">
                        <div class="form-group">
                            <label class="col-sm-2 control-label">课程编号</label>
                            <div class="col-sm-10">
                                <input type="text" disabled value="${classInfo.claNumber}"
                                       name="claNumber" class="form-control">

                            </div>
                        </div>
                        <div class="form-group ">
                            <input type="text" name="claNumber" value="${classInfo.claNumber}" hidden="hidden">
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">课程名称</label>
                            <div class="col-sm-10">
                                <input type="text" value="${classInfo.claName}"
                                       name="claName" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="table1111" style="height: 10px;"></div>
                    <div class="row table11">
                        <button class="btn btn-w-m btn-danger" type="submit">确认修改</button>
                        <button class="btn btn-w-m btn-success hideEditTable" type="button">取消修改</button>
                    </div>
                </form>
            </div>
            <div class="col-1"></div>
        </div>
    </div>


</div>


<style>
    .editTable {
        width: 800px;
        height: 500px;
        background-color: gainsboro;
        display: none;
        position: absolute;

        border-radius: 20px;
        margin: auto;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }

    .addTable {
        width: 800px;
        height: 350px;
        background-color: gainsboro;
        display: none;
        position: absolute;

        border-radius: 20px;
        margin: auto;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }

</style>
<script>
    $(document).ready(function () {
        $(".showAdd").click(function () {
            $(".addTable").fadeIn();
        });
        $(".hideAdd").click(function () {
            $(".addTable").fadeOut();
        });
        $(".showEditTable").click(function () {
            $(".editTable").fadeIn();
        });
        $(".hideEditTable").click(function () {
            $(".editTable").fadeOut();
        });
    });


    function addStuToClass() {


        $.ajax({
            //请求方式
            type: "POST",
            //文件位置
            url: "${contextPath}/admin/showStuInfoList",
            //返回数据格式为json,也可以是其他格式如
            dataType: "json",
            //请求成功后要执行的函数，拼接html
            success: function (data) {
                var str = "<div class=\"col-sm-10\"> ";
                for (var i = 0; i < data.length; i++) {
                    str += ' <div class="radio"> <label> <input type="radio"  name="stuNumber" value="' + data[i].stuNumber + '">' + data[i].stuNumber + ":" + data[i].stuName + ' </input>    </div> </label> '
                }
                str += "</div>"
                $(".addStuToClassInput").append(str);
                console.log(str)
            }
        });
    }
</script>

</body>

</html>

