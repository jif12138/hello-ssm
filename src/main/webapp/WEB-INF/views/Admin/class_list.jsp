<html>
<head>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

    <link href="../statics/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="../statics/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="../statics/css/animate.css" rel="stylesheet">
    <link href="../statics/css/style.css?v=4.1.0" rel="stylesheet">

    <!-- 全局js -->

    <script src="../statics/js/jquery.min.js?v=2.1.4"></script>
    <script src="../statics/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="../statics/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../statics/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../statics/js/plugins/layer/layer.min.js"></script>

    <!-- 自定义js -->
    <script src="../statics/js/hplus.js?v=4.1.0"></script>
    <script type="text/javascript" src="../statics/js/contabs.js"></script>

    <!-- 第三方插件 -->
    <!-- jQuery Validation plugin javascript-->
    <script src="../statics/js/plugins/validate/jquery.validate.min.js"></script>
    <script src="../statics/js/plugins/validate/messages_zh.min.js"></script>
    <script src="../statics/js/demo/form-validate-demo.js"></script>

    <title>班级列表</title>
</head>
<body class="gray-bg top-navigation">

<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
        <!-- 顶部导航栏 -->
        <div class="row border-bottom white-bg">
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-header">
                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse"
                            class="navbar-toggle collapsed" type="button">
                        <i class="fa fa-reorder"></i>
                    </button>
                    <a href="#" class="navbar-brand">${currentAdmin.adminName}</a>
                </div>
                <div class="navbar-collapse collapse" id="navbar">
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a aria-expanded="false" role="button"
                               href="${contextPath}/admin/index?userNumber=${currentAdmin.adminNumber}"> 返回首页</a>
                        </li>

                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 用户管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li><a href="${contextPath}/admin/showTeacher">教师用户管理</a>
                                </li>
                                <li><a href="${contextPath}/admin/showStudent">学生用户管理</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 班级管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li>
                                    <a href="${contextPath}/admin/showAllClass?type=class">查看班级列表</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 课程管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li>
                                    <a href="${contextPath}/admin/showAllCourse">查看课程列表</a>
                                </li>

                            </ul>
                        </li>

                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 成绩管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li>
                                    <a href="${contextPath}/admin/showAllClass?type=score">查看班级列表</a>
                                </li>

                            </ul>
                        </li>

                    </ul>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a href="${contextPath}/login/logOut">
                                <i class="fa fa-sign-out"></i> 注销
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>


        <!-- 中部内容 -->
        <div class="wrapper wrapper-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>班级列表</h5>


                            </div>
                            <div class="ibox-content">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>编号</th>
                                        <th>班级名称</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${classInfos}" var="a">
                                        <tr>
                                            <td>${a.claNumber}</td>
                                            <td>${a.claName}</td>
                                            <td>
                                                <a href="${contextPath}/admin/showThisClassDetail?claNumber=${a.claNumber} ">
                                                    查看班级详细情况</a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>
        </div>


        <!-- 底部 -->
        <div class="footer">
            <div class="pull-right">
                By：<a href="http://www.zi-han.net" target="_blank">zihan's blog</a>
            </div>
            <div>
                <strong>Copyright</strong> H+ &copy; 2014
            </div>
        </div>


        <%-- 添加表单2--%>
        <div class="ibox-content  addTable">
            <form class="form-horizontal m-t" id="signupForm" action="addTeacher" method="post">
                <div class="form-group">
                    <label class="col-sm-3 control-label">编号：</label>
                    <div class="col-sm-8">
                        <input id="teaNumber" name="teaNumber" class="form-control" type="text" aria-required="true">
                        <span class="help-block m-b-none"><i class="fa fa-info-circle"></i>
                            编号一经设定无法修改！</span>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">姓名：</label>
                    <div class="col-sm-8">
                        <input id="teaName" name="teaName" class="form-control" type="text" aria-required="true"
                               aria-invalid="false" class="valid">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">密码：</label>
                    <div class="col-sm-8">
                        <input id="teaPassword" name="teaPassword" class="form-control" type="password">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">确认密码：</label>
                    <div class="col-sm-8">
                        <input id="confirm_teaPassword" name="confirm_teaPassword" class="form-control" type="password">
                        <span class="help-block m-b-none"><i class="fa fa-info-circle"></i> 请再次输入您的密码</span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" class="checkbox" id="agree" name="agree"> 我确认以上信息正确无误！
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-8 col-sm-offset-3">
                        <button class="btn btn-primary btn-danger" type="submit">提交</button>
                        <button class="btn btn-primary  btn-success hideAdd" type="button">取消</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<style>
    .addTable {
        width: 800px;
        height: 350px;
        background-color: gainsboro;
        display: none;
        position: absolute;

        border-radius: 20px;
        margin: auto;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }

</style>

<script>

    $(document).ready(function () {

        $(".showAdd").click(function () {
            $(".addTable").fadeIn();
        });
        $(".hideAdd").click(function () {
            $(".addTable").fadeOut();
        });
    });
</script>

</body>

</html>
