<html>
<head>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

    <link href="../statics/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="../statics/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="../statics/css/animate.css" rel="stylesheet">
    <link href="../statics/css/style.css?v=4.1.0" rel="stylesheet">

    <!-- 全局js -->

    <script src="../statics/js/jquery.min.js?v=2.1.4"></script>
    <script src="../statics/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="../statics/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../statics/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../statics/js/plugins/layer/layer.min.js"></script>

    <!-- 自定义js -->
    <script src="../statics/js/hplus.js?v=4.1.0"></script>
    <script type="text/javascript" src="../statics/js/contabs.js"></script>

    <!-- 第三方插件 -->
    <!-- jQuery Validation plugin javascript-->
    <script src="../statics/js/plugins/validate/jquery.validate.min.js"></script>
    <script src="../statics/js/plugins/validate/messages_zh.min.js"></script>
    <script src="../statics/js/demo/form-validate-demo.js"></script>

    <title>成绩列表</title>
</head>
<body class="gray-bg top-navigation">

<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">
        <!-- 顶部导航栏 -->
        <div class="row border-bottom white-bg">
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-header">
                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse"
                            class="navbar-toggle collapsed" type="button">
                        <i class="fa fa-reorder"></i>
                    </button>
                    <a href="#" class="navbar-brand">${currentAdmin.adminName}</a>
                </div>
                <div class="navbar-collapse collapse" id="navbar">
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a aria-expanded="false" role="button"
                               href="${contextPath}/admin/index?userNumber=${currentAdmin.adminNumber}"> 返回首页</a>
                        </li>

                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 用户管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li><a href="${contextPath}/admin/showTeacher">教师用户管理</a>
                                </li>
                                <li><a href="${contextPath}/admin/showStudent">学生用户管理</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 班级管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li>
                                    <a href="${contextPath}/admin/showAllClass?type=class">查看班级列表</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 课程管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li>
                                    <a href="${contextPath}/admin/showAllCourse">查看课程列表</a>
                                </li>

                            </ul>
                        </li>

                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 成绩管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li>
                                    <a href="${contextPath}/admin/showAllClass?type=score">查看班级列表</a>
                                </li>

                            </ul>
                        </li>

                    </ul>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a href="${contextPath}/login/logOut">
                                <i class="fa fa-sign-out"></i> 注销
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>

        <!-- 中部内容 -->
        <div class="wrapper wrapper-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-10">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>成绩列表</h5>
                                <hr/>
                                <a class="btn btn-primary showAdd"><i class="glyphicon glyphicon-plus"></i>&nbsp;
                                    添加成绩
                                </a>
                            </div>
                            <div class="ibox-content">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>成绩编号</th>
                                        <th>课程编号</th>
                                        <th>学生编号</th>

                                        <th>操作</th>
                                        <th>操作</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${scoreList}" var="a">
                                        <tr>
                                            <td>${a.scNumber}</td>
                                            <td>${a.scCouNumber}</td>
                                            <td>${a.scStuNumber}</td>
                                                <%--<td>${a.couTeaName}</td>--%>
                                            <td>${a.scScore}</td>

                                            <td>
                                                <a href="${contextPath}/admin/showScoreDetail?scNumber=${a.scNumber}">
                                                    详情</a>
                                            </td>
                                            <td>
                                                <a href="${contextPath}/admin/deleteScore?scNumber=${a.scNumber}">
                                                    删除</a>
                                            </td>


                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>
        </div>


        <!-- 底部 -->
        <div class="footer">
            <div class="pull-right">
                By：<a href="http://www.zi-han.net" target="_blank">zihan's blog</a>
            </div>
            <div>
                <strong>Copyright</strong> H+ &copy; 2014
            </div>
        </div>


        <%-- 添加表单2--%>
        <div class="ibox-content  addTable">
            <form class="form-horizontal m-t" id="signupForm" action="${contextPath}/admin/addScore" method="post">
                <div class="form-groups">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">成绩编号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="scNumber">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">课程编号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="scCouNumber">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">学生编号</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="scStuNumber">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">成绩</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="scScore">

                        </div>
                    </div>

                </div>
                <div class="row table11">
                    <button class="btn btn-w-m btn-danger" type="submit">确认修改</button>
                    <button class="btn btn-w-m btn-success hideAdd" type="button">取消修改</button>
                </div>

            </form>
        </div>
    </div>
</div>
<style>
    .addTable {
        width: 800px;
        height: 350px;
        background-color: gainsboro;
        display: none;
        position: absolute;

        border-radius: 20px;
        margin: auto;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }

    .table1111 {
        weight: 800px;
        height: 100px;

    }

    .table11 {
        margin-top: 200px;
        display: flex;
        flex-direction: row;
        justify-content: space-between;

    }

    .form-groups {
        margin-left: 80px;
    }

    .ttt1 {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
    }
</style>

<script>

    $(document).ready(function () {

        $(".showAdd").click(function () {
            $(".addTable").fadeIn();
        });
        $(".hideAdd").click(function () {
            $(".addTable").fadeOut();
        });
    });
</script>

</body>

</html>
