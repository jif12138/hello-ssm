<html>
<head>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <script src="../statics/js/jquery.min.js?v=2.1.4"></script>

    <link href="../statics/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="../statics/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="../statics/css/animate.css" rel="stylesheet">
    <link href="../statics/css/style.css?v=4.1.0" rel="stylesheet">

    <!-- 全局js -->

    <script src="../statics/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="../statics/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../statics/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../statics/js/plugins/layer/layer.min.js"></script>

    <!-- 自定义js -->
    <script src="../statics/js/hplus.js?v=4.1.0"></script>
    <script type="text/javascript" src="../statics/js/contabs.js"></script>

    <!-- 第三方插件 -->
    <!-- jQuery Validation plugin javascript-->
    <script src="../statics/js/plugins/validate/jquery.validate.min.js"></script>
    <script src="../statics/js/plugins/validate/messages_zh.min.js"></script>
    <script src="../statics/js/demo/form-validate-demo.js"></script>

    <title>课程详情</title>
</head>
<body class="gray-bg top-navigation">

<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">

        <!-- 顶部导航栏 -->
        <div class="row border-bottom white-bg">
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-header">
                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse"
                            class="navbar-toggle collapsed" type="button">
                        <i class="fa fa-reorder"></i>
                    </button>
                    <a href="#" class="navbar-brand">${currentAdmin.adminName}</a>
                </div>
                <div class="navbar-collapse collapse" id="navbar">
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a aria-expanded="false" role="button"
                               href="${contextPath}/admin/index?userNumber=${currentAdmin.adminNumber}"> 返回首页</a>
                        </li>

                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 用户管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li><a href="${contextPath}/admin/showTeacher">教师用户管理</a>
                                </li>
                                <li><a href="${contextPath}/admin/showStudent">学生用户管理</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 班级管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li>
                                    <a href="${contextPath}/admin/showAllClass?type=class">查看班级列表</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 课程管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li>
                                    <a href="${contextPath}/admin/showAllCourse">查看课程列表</a>
                                </li>

                            </ul>
                        </li>

                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 成绩管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li>
                                    <a href="${contextPath}/admin/showAllClass?type=score">查看班级列表</a>
                                </li>

                            </ul>
                        </li>

                    </ul>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a href="${contextPath}/login/logOut">
                                <i class="fa fa-sign-out"></i> 注销
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>

        <!-- 中部内容 -->
        <div class="wrapper wrapper-content">
            <div class="container">
                <div class="container">
                    <div class="wrapper wrapper-content">
                        <div class="col-md-4"></div>

                        <div class="col-md-4">
                            <div class="widget-head-color-box navy-bg p-lg text-center">
                                <div class="m-b-md">

                                    <h2 class="font-bold no-margins">
                                        课程详情
                                    </h2>
                                    <small></small>
                                </div>
                            </div>
                            <div class="widget-text-box">
                                <h4 class="media-heading">
                                    课程编号：【${courseInfo.couNumber}】
                                </h4>

                                课程名称：【 <span>${courseInfo.couName}</span> 】
                                <br>
                                授课教师编号：【<span> ${courseInfo.couTeaNumber} </span>】
                                <br>
                                课程学时：【<span> ${courseInfo.couHour} </span>】
                                <br>
                                课程学分：【<span> ${courseInfo.couCredit} </span>】
                                <br>
                                课程专业：【<span> ${courseInfo.couMajor} </span>】
                                <br>
                                课程地址：【<span> ${courseInfo.couAddress} </span>】
                                <br>

                                <div class="text-right">
                                    <a class="btn btn-xs btn-primary showEditTable"><i class="fa fa-edit"></i> 修改</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4"></div>
                    </div>


                </div>
            </div>


            <!-- 底部 -->
            <div class="footer">
                <div class="pull-right">
                    By：<a href="http://www.zi-han.net" target="_blank">zihan's blog</a>
                </div>
                <div>
                    <strong>Copyright</strong> H+ &copy; 2014
                </div>
            </div>


            <!--修改表单-->
            <div class="row editTable">
                <div class="col-1"></div>
                <div class="col-md-10 ">
                    <h1>修改信息</h1>
                    <form action="${contextPath}/admin/updateCourse" method="post">
                        <div class="form-groups">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">课程编号</label>
                                <div class="col-sm-10">
                                    <input type="text" value="${courseInfo.couNumber}" class="form-control"
                                           name="couNumber"
                                           disabled="">
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="text" hidden value="${courseInfo.couNumber}" name="couNumber">
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">课程名称</label>
                                <div class="col-sm-10">
                                    <input type="text" value="${courseInfo.couName}"
                                           class="form-control" name="couName">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">授课教师编号</label>
                                <div class="col-sm-10">
                                    <input type="text" value="${courseInfo.couTeaNumber}"
                                           class="form-control" name="couTeaNumber">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label">课程学时</label>
                                <div class="col-sm-10">
                                    <input type="text" value="${courseInfo.couHour}"
                                           class="form-control" name="couHour">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">课程学分</label>
                                <div class="col-sm-10">
                                    <input type="text" value="${courseInfo.couCredit}"
                                           class="form-control" name="couCredit">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">课程专业</label>
                                <div class="col-sm-10">
                                    <input type="text" value="${courseInfo.couMajor}"
                                           class="form-control" name="couMajor">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">课程地址</label>
                                <input type="text" value="${courseInfo.couAddress}"
                                       class="form-control" name=" couAddress">

                            </div>
                        </div>
                        <div class="table1111" style="height: 10px;"></div>
                        <div class="row table11">
                            <button class="btn btn-w-m btn-danger" type="submit">确认修改</button>
                            <button class="btn btn-w-m btn-success hideEditTable" type="button">取消修改</button>
                        </div>

                    </form>
                </div>
                <div class="col-1"></div>
            </div>

        </div>
    </div>
</div>
<style>
    .editTable {
        width: 950px;
        height: 500px;
        background-color: gainsboro;
        display: none;
        position: absolute;

        border-radius: 20px;
        margin: auto;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }

    .table1111 {
        weight: 800px;
        height: 100px;

    }

    .table11 {
        margin-top: 200px;
        display: flex;
        flex-direction: row;
        justify-content: space-between;

    }

    .form-groups {
        margin-left: 80px;
    }

    .ttt1 {
        display: flex;
        flex-direction: row;
        justify-content: space-between;
    }

</style>
<script>
    $(document).ready(function () {

        $(".showEditTable").click(function () {
            $(".editTable").fadeIn();
        });
        $(".hideEditTable").click(function () {
            $(".editTable").fadeOut();
        });
    });
</script>
</body>

</html>
