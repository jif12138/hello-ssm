<html>
<head>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

    <link href="../statics/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="../statics/css/font-awesome.min.css?v=4.4.0" rel="stylesheet">
    <link href="../statics/css/animate.css" rel="stylesheet">
    <link href="../statics/css/style.css?v=4.1.0" rel="stylesheet">


    <!-- 全局js -->
    <script src="../statics/js/jquery.min.js?v=2.1.4"></script>
    <script src="../statics/js/bootstrap.min.js?v=3.3.6"></script>
    <script src="../statics/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="../statics/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="../statics/js/plugins/layer/layer.min.js"></script>

    <!-- 自定义js -->
    <script src="../statics/js/hplus.js?v=4.1.0"></script>
    <script type="text/javascript" src="../statics/js/contabs.js"></script>

    <!-- 第三方插件 -->
    <script src="../statics/js/plugins/pace/pace.min.js"></script>

    <title>学生列表</title>
</head>
<body class="gray-bg top-navigation">

<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">


        <!-- 顶部导航栏 -->
        <div class="row border-bottom white-bg">
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-header">
                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse"
                            class="navbar-toggle collapsed" type="button">
                        <i class="fa fa-reorder"></i>
                    </button>
                    <a href="#" class="navbar-brand">${currentAdmin.adminName}</a>
                </div>
                <div class="navbar-collapse collapse" id="navbar">
                    <ul class="nav navbar-nav">
                        <li class="active">
                            <a aria-expanded="false" role="button"
                               href="${contextPath}/admin/index?userNumber=${currentAdmin.adminNumber}"> 返回首页</a>
                        </li>

                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 用户管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li><a href="${contextPath}/admin/showTeacher">教师用户管理</a>
                                </li>
                                <li><a href="${contextPath}/admin/showStudent">学生用户管理</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 班级管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li>
                                    <a href="${contextPath}/admin/showAllClass?type=class">查看班级列表</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 课程管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li>
                                    <a href="${contextPath}/admin/showAllCourse">查看课程列表</a>
                                </li>

                            </ul>
                        </li>

                        <li class="dropdown">
                            <a aria-expanded="false" role="button" href="#" class="dropdown-toggle"
                               data-toggle="dropdown"> 成绩管理 <span class="caret"></span></a>
                            <ul role="menu" class="dropdown-menu">
                                <li>
                                    <a href="${contextPath}/admin/showAllClass?type=score">查看班级列表</a>
                                </li>

                            </ul>
                        </li>

                    </ul>
                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a href="${contextPath}/login/logOut">
                                <i class="fa fa-sign-out"></i> 注销
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>

        <!-- 中部内容 -->
        <div class="wrapper wrapper-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-2"></div>
                    <div class="col-sm-8">

                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>学生列表</h5>
                                <hr>
                                <a class="btn btn-primary showAdd"><i class="glyphicon glyphicon-plus"></i>&nbsp;
                                    添加学生
                                </a>
                            </div>
                            <div class="ibox-content">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>编号</th>
                                        <th>姓名</th>
                                        <th>性别</th>
                                        <th>专业</th>
                                        <th>学校</th>
                                        <th>电话</th>
                                        <th>操作</th>

                                        <th>操作</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${students}" var="oneStu">
                                        <tr>
                                            <td>${oneStu.stuNumber}</td>
                                            <td> ${oneStu.stuName}</td>
                                            <td> ${oneStu.stuGender}</td>
                                            <td> ${oneStu.stuMajor}</td>
                                            <td> ${oneStu.stuClassNumber}</td>
                                            <td> ${oneStu.stuTelephone}</td>
                                            <td>
                                                <a href="${contextPath}/admin/showStudentDetail?stuNumber=${oneStu.stuNumber}">
                                                    详情</a></td>

                                            <td>
                                                <a href="${contextPath}/admin/deleteStudent?stuNumber=${oneStu.stuNumber}">
                                                    删除</a></td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-2"></div>
                </div>
            </div>
        </div>


        <!-- 底部 -->
        <div class="footer">
            <div class="pull-right">
                By：<a href="http://www.zi-han.net" target="_blank">zihan's blog</a>
            </div>
            <div>
                <strong>Copyright</strong> H+ &copy; 2014
            </div>
        </div>
        <!-- 添加表单 -->
        <div class="row addTable">
            <div class="col-1"></div>
            <div class="col-md-10 ">
                <h1>添加学生</h1>

                <div class="row">
                    <div class="col-md-2"></div>
                    <div class="col-md-8">

                        <form action="${contextPath}/admin/addStudent" method="post">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">编号</label>
                                <div class="col-sm-10">
                                    <input type="text" value="${studentInfo.stuNumber}"
                                           name="stuNumber" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">密码</label>
                                <div class="col-sm-10">
                                    <input type="password" value="${studentInfo.stuPassword}"
                                           name="stuPassword" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">姓名</label>
                                <div class="col-sm-10">
                                    <input type="text" value="${studentInfo.stuName}"
                                           name="stuName" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">性别</label>
                                <div class="col-sm-10">
                                    <input type="text" value="${studentInfo.stuGender}"
                                           name="stuGender" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">专业</label>
                                <div class="col-sm-10">
                                    <input type="text" value="${studentInfo.stuMajor}"
                                           name="stuMajor" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">班级编号</label>
                                <div class="col-sm-10">
                                    <input type="text" value="${studentInfo.stuClassNumber}"
                                           name="stuClassNumber" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">电话</label>
                                <div class="col-sm-10">
                                    <input type="text" value="${studentInfo.stuTelephone}"
                                           name="stuTelephone" class="form-control">
                                </div>
                            </div>
                            <button class="btn  btn-w-m btn-danger" style=" margin-top: 20px;" type="submit">确认添加
                            </button>
                            <button class="btn btn-w-m btn-success hideAdd" style=" margin-top: 20px;margin-left: 90px"
                                    type="button">取消添加
                            </button>
                        </form>
                    </div>
                    <div class="col-md-2"></div>
                </div>
            </div>
            <div class="col-1"></div>
        </div>

    </div>
</div>
<style>
    .addTable {
        width: 800px;
        height: 350px;
        background-color: gainsboro;
        display: none;
        position: absolute;

        border-radius: 20px;
        margin: auto;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
    }

</style>

<script>

    $(document).ready(function () {

        $(".showAdd").click(function () {
            $(".addTable").fadeIn();
        });
        $(".hideAdd").click(function () {
            $(".addTable").fadeOut();
        });
    });
</script>


</body>

</html>
