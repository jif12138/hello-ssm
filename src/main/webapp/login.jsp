<!DOCTYPE html>
<html>

<head>

    <%@ page language="java" import="java.util.*" pageEncoding="UTF-8" %>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">


    <title>登录</title>
    <meta name="keywords" content="H+后台主题,后台bootstrap框架,会员中心主题,后台HTML,响应式后台">
    <meta name="description" content="H+是一个完全响应式，基于Bootstrap3最新版本开发的扁平化主题，她采用了主流的左右两栏式布局，使用了Html5+CSS3等现代技术">

    <link rel="shortcut icon" href="favicon.ico">
    <link href="statics/css/bootstrap.min.css?v=3.3.6" rel="stylesheet">
    <link href="statics/css/font-awesome.css?v=4.4.0" rel="stylesheet">

    <link href="statics/css/animate.css" rel="stylesheet">
    <link href="statics/css/style.css?v=4.1.0" rel="stylesheet">

    <script>if (window.top !== window.self) {
        window.top.location = window.location;
    }</script>


    <meta charset="UTF-8">
    <title>test</title>
</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen  animated fadeInDown">
    <div>
        <div>

            <%--<h1 class="logo-name">成绩管理系统</h1>--%>

        </div>
        <h3>欢迎使用 成绩管理系统</h3>

        <form class="m-t" role="form" action="login/login.do" method="post">
            <div class="form-group">
                <input type="text" name="usernumber" value="1" class="form-control" placeholder="账号" required="">
            </div>
            <div class="form-group">
                <input type="password" name="password" value="123" class="form-control" placeholder="密码" required="">
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">登 录</button>

        </form>
    </div>
</div>

<!-- 全局js -->
<script src="js/jquery.min.js?v=2.1.4"></script>
<script src="js/bootstrap.min.js?v=3.3.6"></script>

<script type="text/javascript" src="http://tajs.qq.com/stats?sId=9051096" charset="UTF-8"></script>
<!--统计代码，可删除-->

</body>

</html>
