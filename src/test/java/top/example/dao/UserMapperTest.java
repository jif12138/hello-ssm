package top.example.dao;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import top.example.mapper.UserMapper;
import top.example.model.User;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * project helloSSM
 * authod wuyanhui
 * datetime 2017/11/23 16:45
 * desc
 */

// 加载spring配置文件
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath:config/spring-mybatis.xml"})
public class UserMapperTest {

    @Autowired
    private UserMapper dao;

    @Test
    public void testSelectUser() throws Exception {


    }

    @Test
    public void redisTest() {

    }


    @Test
    public void testList() throws IOException {
        //加载mybatis核心配置文件
        InputStream is = Resources.getResourceAsStream("config/spring-mybatis.xml");
        //构建sqlSessionFactory工厂对象
        SqlSessionFactory factory = new SqlSessionFactoryBuilder().build(is);
        //通过工厂对象打开sqlsession
        SqlSession session = factory.openSession();
        //通过session执行查询操作
        List<User> list = session.selectList("mapper/userDao.xml.findOneAdminByName");
        for (User user : list) {
            System.out.println(user);
        }
        session.close();
    }

}